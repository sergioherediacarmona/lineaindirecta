/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 11-17-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
**/
import { LightningElement, track } from 'lwc';
import createCommunityUser from '@salesforce/apex/CHRegisterHandler.createCommunityUser'
import getFieldSetRegister from '@salesforce/apex/CHRegisterHandler.getFieldSetRegister'
import COMMUNITY_LOGO from '@salesforce/resourceUrl/lineadirecta';

export default class ChRegister extends LightningElement {

    @track regSuccess = false;
    @track guestUser = {};
    @track policies = false;
    @track toastMessage = '';
    @track opacity;
    @track fields = [];
    comunityLogo = COMMUNITY_LOGO

    get opacityStyle() { return 'opacity: ' + this.opacity }

    connectedCallback(){
        debugger;
        this.getFieldSetRegisterJS();
    }

    inputChange(evt){
        this.guestUser[evt.target.dataset.input] = evt.target.value
    }

    handleClick(){
        createCommunityUser({user : this.guestUser})
            .then((res) => {
                if (res != "success"){
                    this.showToast(res, 'error')
                }else{
                    this.regSuccess = true
                }
            })
            .catch((error) => {
                console.error("Error on CreateCommunityUser: " + error + ' / ' + JSON.stringify(error))
            })
    }

    getFieldSetRegisterJS(){
        getFieldSetRegister()
            .then((res) => {
                this.fields = res;
            })
            .catch((error) => {
                console.error("Error on CreateCommunityUser: " + error + ' / ' + JSON.stringify(error))
            })
    }

    showToast(msg, variant) {
        this.toastMessage = 'Error en el registro: ' + msg
        this.variantMessage = variant
        this.opacity = 1
        let temp = setInterval(() => {
            this.setOpacity()
            clearInterval(temp)
        }, 2500)
    }

    setOpacity() {
        let fadeEffect = setInterval(() => {
            if(this.opacity > 0) {
                this.opacity -= 0.02
            } else {
                this.toastMessage = ''
                clearInterval(fadeEffect)
            }
        }, 20)
    }

}