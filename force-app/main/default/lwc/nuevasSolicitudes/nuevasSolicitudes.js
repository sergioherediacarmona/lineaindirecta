/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 11-19-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
**/
import { LightningElement, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getTypeDependentFields from '@salesforce/apex/SolicitudesHandler.getTypeDependentFields'
import createApplication from '@salesforce/apex/SolicitudesHandler.createApplication'

export default class NuevasSolicitudes extends LightningElement {
    object = 'Case';
    masterField = {apiName : 'Type', required : true};
    @track dependedFields = [];
    @track newApplication = {};

    typeChanged(event){
        this.event = event;
        this.fieldChanged(event);
        this.getTypeDependentFieldsJS();
    }

    fieldChanged(event){
        this.newApplication[event.target.fieldName] = event.target.value;
    }

    getTypeDependentFieldsJS(){
        getTypeDependentFields({fieldSet : this.event.target.value})
        .then(res => {
            this.dependedFields = res;
        })
        .catch(err => {
            this.error = err;
        })
    }

    get checkApplicationFields(){
        if (this.newApplication[this.masterField.apiName] && this.newApplication[this.masterField.apiName] !== ''){
            var isValid = true;

            this.dependedFields.forEach(field => {
                if (field.required){
                    isValid = isValid && this.newApplication[field.apiName] && this.newApplication[field.apiName] !== '';
                }
            });

            return isValid;
        }else{
            return false;
        }
    }

    createApplicationJS(event){
        if (this.checkApplicationFields){
            createApplication( {application : this.newApplication} )
            .then(res => {
                this.dependedFields = [];
                this.dependedFields = [...this.dependedFields];
                this.sendToast('Solicitud creada existosamente', '', 'success')
            })
            .catch(err => {                
                this.sendToast('Fallo en la creación de la solicitud', '', 'error')
                this.error = err;
            })
        }else{
            this.sendToast('Debe rellenar todos los campos', 'Si hay alguno por defecto, pruebe modificar el valor a uno provisional y luego al que había por defecto.', 'warning')
        }
    }

    sendToast(title, message, variant){
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant
        });
        this.dispatchEvent(evt);
    }
}