/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 11-18-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
**/
import { LightningElement, track } from 'lwc';
import getMisSolicitudesColumn from '@salesforce/apex/SolicitudesHandler.getMisSolicitudesColumn'
import getMisSolicitudesData from '@salesforce/apex/SolicitudesHandler.getMisSolicitudesData'

export default class MisSolicitudes extends LightningElement {

    // example1Columns = [
    //     { fieldName : "Id", label: "Id" },
    //     { fieldName : "Name", label: "Name" },
    //     { fieldName : "Subname", label: "Subnamee" },
    //     { fieldName : "Tipo", label: "Tipo" }
    // ]

    // example1Rows = [
    //     {Id: "a1x6E000000F9AWQA0", Tipo: "Recurso", Name: "Atencion_Cliente_Todos_Centros", Subname: "Atencion_Cliente" },
    //     {Id: "a1x6E000000F9B3QAK", Tipo: "Recurso", Name: "Atencion_Cliente_ALICANTE", Subname: "Atencion_Cl" }, 
    //     {Id: "a1x6E000000F9B8QAK", Tipo: "Recurso", Name: "Atencion_Cliente_BARCELONA", Subname: "Atencion_Cli" }, 
    //     {Id: "a1x6E000000F9BDQA0", Tipo: "Recurso", Name: "Atencion_Cliente_CENTRAL", Subname: "Atencion_C" }, 
    //     {Id: "a1x6E000000F9BDQA0", Tipo: "Telefono", Name: "Atencion_Cliente_OVIEDO", Subname: "Atencion_" }
    // ]

    @track columns = [];
    @track rows = [];

    connectedCallback(){
        debugger
        this.getMisSolicitudesColumnJS();
        this.getMisSolicitudesDataJS();
    }

    getMisSolicitudesColumnJS(){
        getMisSolicitudesColumn()
        .then(res => {
            this.columns = res;
        })
        .catch(err => {
            this.error = err;
        })
    }

    getMisSolicitudesDataJS(){
        getMisSolicitudesData()
        .then(res => {
            this.rows = res;
        })
        .catch(err => {
            this.error = err;
        })
    }

}