/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 11-19-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
**/
import { LightningElement, track } from 'lwc';
import { subscribe, unsubscribe, onError, setDebugFlag, isEmpEnabled} from "lightning/empApi";

export default class NewCaseListener extends LightningElement {

    channelName = '/data/CaseChangeEvent';
    subscription = {};
    @track text;

    connectedCallback(){
        this.text = 'Esperando mensajes';
        this.handleSubscribe();
    }

    disconnectedCallback(){
        this.handleUnsubscribe();
    }

    handleMessageReceived(){
        this.dispatchEvent(new CustomEvent('newcasecreated', {detail: {Id : this.applicationId}} ));
        this.text = 'Recibido evento CDC con ID: ' + this.applicationId;
    }

    handleSubscribe() {
        // Callback invoked whenever a new event message is received
        const messageCallback = (response) => {
            if (response.data.payload.ChangeEventHeader.changeType === 'CREATE'){
                this.applicationId = response.data.payload.ChangeEventHeader.recordIds[0]
                this.handleMessageReceived();
            }
            console.log('New message received: ', JSON.stringify(response));
            // Response contains the payload of the new message received
        };

        // Invoke subscribe method of empApi. Pass reference to messageCallback
        subscribe(this.channelName, -1, messageCallback).then((response) => {
            // Response contains the subscription information on subscribe call
            console.log('Subscription request sent to: ', JSON.stringify(response.channel));
            this.subscription = response;
        });
    }

    handleUnsubscribe() {   
        // Invoke unsubscribe method of empApi
        unsubscribe(this.subscription, (response) => {
            console.log('unsubscribe() response: ', JSON.stringify(response));
            // Response is true for successful unsubscribe
        });
    }

}