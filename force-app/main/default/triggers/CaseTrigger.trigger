/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 11-18-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
**/
trigger CaseTrigger on Case (before insert, before update) {

    if (Trigger.isBefore && Trigger.isInsert){
        CaseTriggerHandler.changeOwner();
    }
    if (Trigger.isBefore && Trigger.isUpdate){
        CaseTriggerHandler.recoverContactId();
    }

}