/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 11-19-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
**/
({
    openTab : function(component, event, helper) {
        var workspaceAPI = component.find("workspace");
        workspaceAPI.openTab({
            url: '#/sObject/5007Q0000014DfIQAU/view',
            focus: true
        });
    },
    handleNewCasCreated: function(component, event) {
        var workspaceAPI = component.find("workspace");
        var caseId = event.getParam('Id');

        workspaceAPI.openTab({
            url: '#/sObject/' + caseId + '/view',
            focus: true
        });
    },
})