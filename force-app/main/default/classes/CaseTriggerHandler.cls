/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 11-18-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
**/
public inherited sharing class CaseTriggerHandler {
    
    public static void changeOwner(){
        //Obtenemos los usuarios de servicio
        List<User> users = [SELECT Id, Username FROM User WHERE USERPERMISSIONSSUPPORTUSER = true];
        
        //Obtenemos el nº de casos asignados agrupados por OwnerId
        AggregateResult[] nCasosByServiceUser = [SELECT OwnerId, COUNT(Id) nCases FROM Case WHERE OwnerId IN: users GROUP BY OwnerId];

        //Creamos un mapa en donde contar el nº de casos asignados a un usuario de servicio
        Map<Id, Integer> casosAsignados = new Map<Id, Integer>();
        
        //Inicializamos el mapa
        for (User user : users){
            casosAsignados.put(user.Id, 0);
        }

        //Ajustamos el mapa según la informacion de los casos obtenida
        for (AggregateResult ar : nCasosByServiceUser)  {
            casosAsignados.put( (String) ar.get('OwnerId'), (Integer) ar.get('nCases'));
        }

        //Recorremos cada caso a insertar
        for (Case cas : (List<Case>) Trigger.new){
            //Obtenemos el usuario con el menor nº de casos asignados
            ID minServiceUSer = getMinServiceUser(casosAsignados);
            
            //Si no existe tal usuario no hago nada
            if (minServiceUSer != null){
                //En caso contrario cambio el OwnerId y actualizo el contador de casos asignados
                cas.OwnerId = minServiceUSer;
                casosAsignados.put(minServiceUSer, casosAsignados.get(minServiceUSer) + 1);
            }
        }

    }

    public static void recoverContactId(){
        for (Case cas : (List<Case>) Trigger.new){
            cas.ContactId = ((Case) Trigger.oldMap.get(cas.Id)).ContactId;
        }
    }
    
    public static ID getMinServiceUser(Map<Id, Integer> casosAsignados){

        //Si no hay usarios devuelvo nulo
        if(casosAsignados == null || casosAsignados.isEmpty()){
            return null;
        }else{
            //En caso contrario recorro el contador y obtengo el usuario de servicio con el menor nº de casos asignados
            ID minServiceUSer;
            Integer nCasos = 99999999;

            for (Id user : casosAsignados.keySet()){
                if (nCasos > casosAsignados.get(user)){
                    nCasos = casosAsignados.get(user);
                    minServiceUSer = user;
                }
            }

            return minServiceUSer;
        }

    }

}
