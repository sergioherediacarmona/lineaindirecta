/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 11-18-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
**/
public with sharing class SolicitudesHandler {
    
    @AuraEnabled
    public static List<Map<String,Object>> getTypeDependentFields(String fieldSet){
        fieldSet = fieldSet.replace(' ', '_').replace('á', 'a').replace('é', 'e').replace('í', 'i').replace('ó', 'o').replace('ú', 'u');
        
        List<Map<String,Object>> result = new List<Map<String,Object>>();

        List<Schema.FieldSetMember> fields = OM_SchemaUtils.getFieldSet('Case', fieldSet);

        for(Schema.FieldSetMember field : fields){
            Map<String,Object> aux = new Map<String,Object>();
            aux.put('apiName', field.getFieldPath());
            aux.put('required', field.getRequired());
            result.add(aux);
        }

        return result;
    }

    @AuraEnabled
    public static List<Map<String,Object>> getMisSolicitudesColumn(){
        List<Map<String,Object>> result = new List<Map<String,Object>>();

        List<Schema.FieldSetMember> fields = OM_SchemaUtils.getFieldSet('Case', 'datatableInfo');

        for(Schema.FieldSetMember field : fields){
            Map<String,Object> aux = new Map<String,Object>();
            aux.put('fieldName', field.getFieldPath().replace('Contact.', '_'));
            aux.put('label', field.getLabel());
            result.add(aux);
        }

        return result;
    }
    
    @AuraEnabled
    public static List<Map<String,Object>> getMisSolicitudesData(){
        User user = [SELECT ID, Contact.Id FROM User WHERE Name =: UserInfo.getName() LIMIT 1];

        List<Map<String,Object>> result = new List<Map<String,Object>>();
        String query = 'SELECT ';

        List<Schema.FieldSetMember> fields = OM_SchemaUtils.getFieldSet('Case', 'datatableInfo');

        for(Schema.FieldSetMember field : fields){
            query += field.getFieldPath() + ', ';
        }

        query = query.Substring(0, query.length() - 2); //Elimino la última coma
        query += ' FROM Case WHERE ContactId = \'' + user.Contact.Id + '\'' ;

        for (Case cas : System.Database.query(query)){
            Map<String,Object> aux = new Map<String,Object>();

            for(Schema.FieldSetMember field : fields){
                if (field.getFieldPath().contains('Contact.')){ //Es un campo de LookUp a Contact
                    aux.put( field.getFieldPath().replace('Contact.', '_'), cas.Contact.get(field.getFieldPath().replace('Contact.', '')) );
                }else{ //Es un campo de Case
                    aux.put( field.getFieldPath(), cas.get(field.getFieldPath()) );
                }
            }

            result.add(aux);
        }

        return result;
    }
    
    @AuraEnabled
    public static string createApplication(Case application){
        User user = [SELECT ID, Contact.Id FROM User WHERE Name =: UserInfo.getName() LIMIT 1];
        application.ContactId = user.Contact.Id;
        insert application;
        return 'OK';
    }
    
}
