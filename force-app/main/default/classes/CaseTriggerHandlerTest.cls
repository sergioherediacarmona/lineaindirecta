/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 11-18-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
**/
@isTest(SeeAllData=true)
public with sharing class CaseTriggerHandlerTest {
    @istest static void CaseTriggerHandlerTest() {
        // Setup test data
        // This code runs as the system user
        Account acc = new Account(Name = 'Name');
        insert acc;

        Contact con = new Contact(LastName = 'LastName', DNI__c = '12345689A', AccountId = acc.id);
        insert con;

        Case cas = new Case( Status = 'new', Origin = 'Phone' );

        Profile pro = [SELECT Id, Name  FROM Profile WHERE Name = 'LineaIndirecta' LIMIT 1];

        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = pro.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com', ContactId = con.Id);

        System.runAs(u){
            Test.startTest();
            SolicitudesHandler.createApplication(cas);
            Test.stopTest();

            List<Case> cases = [ SELECT Id, ContactId, OwnerId FROM Case WHERE ID =: cas.Id LIMIT 1];

            System.assert(cases.size() > 0, 'Debe existir un caso');

            System.assert(cases.get(0).ContactId == con.Id, '' + cases.get(0).ContactId);
            System.assertNotEquals(cases.get(0).OwnerId, u.Id, 'El owner debe cambiar pero no');
        }
    }
}
