/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 11-18-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
**/
public without sharing class CHRegisterHandler {
    
    @AuraEnabled
    public static List<Map<String,Object>> getFieldSetRegister(){
        List<Map<String,Object>> result = new List<Map<String,Object>>();

        for(Schema.FieldSetMember field : OM_SchemaUtils.getFieldSet('Contact', 'registerForm')){
            Map<String,Object> aux = new Map<String,Object>();
            aux.put('apiName', field.getFieldPath());
            aux.put('label', field.getLabel());
            aux.put('required', field.getRequired());
            result.add(aux);
        }

        return result;
    }

    @AuraEnabled
    public static String createCommunityUser(Map<String, String> user){

        Profile pro = [SELECT Id, Name  FROM Profile WHERE Name = 'LineaIndirecta' LIMIT 1];
        List<User> posibleUser = [SELECT Id, Email, Contact.FirstName FROM User WHERE profileId =: pro.Id AND Username =: user.get('Email') LIMIT 1];

        if (posibleUser != null && !posibleUser.isEmpty()){ //Si existe un usuario con el mismo username no lo creo
            return 'Ya existe un usuario con username: ' + user.get('Email');
        }

        String msg;

        List<Map<String,Object>> fields = getFieldSetRegister();

        //Me traigo los posibles contactos con email o dni coincidente
        List<Contact> posibleContact = [SELECT Id, DNI__c, Email, FirstName FROM Contact WHERE DNI__c =: user.get('DNI') OR Email =: user.get('Email') LIMIT 1];
        //Me traigo las posibles polizas con numero de poliza coincidente
        List<Poliza__c> posiblePoliza = [SELECT Id, Contact__c FROM Poliza__c WHERE NumeroPoliza__c =: user.get('NumeroPoliza') AND NumeroPoliza__c != NULL LIMIT 1];

        //Me traigo la cuenta a la que asociar los contactos
        Account account = [SELECT Id FROM Account WHERE Name = 'Linea Indirecta' LIMIT 1];

        //Relleno los datos que tengo para los contactos
        Contact con = new Contact();
        for (Map<String, Object> field : fields){
            if( (Boolean) field.get('required') ){
                con.put( (String) field.get('apiName'), user.get( (String) field.get('label')) );
            }
        }
        con.AccountId = account.Id;

        //Compruebo que se han metido todos los campos requeridos
        if ( !user.isEmpty() ){
            for (Map<String, Object> field : fields){
                if( (Boolean) field.get('required') && String.isBlank(user.get( (String) field.get('label'))) ){
                    msg = 'Por favor, rellene todos los campos requeridos';
                    return msg;
                }
            }
        }else{
            msg = 'Por favor, rellene todos los campos requeridos';
            return msg;
        }

        //Ya existe un contacto
        if ( posibleContact != null && !posibleContact.isEmpty() ){
            con.Id = posibleContact.get(0).Id;
            if (user.containsKey('NumeroPoliza')){ //Ha añadido una póliza
                List<Poliza__c> pol = [SELECT Id, Contact__c FROM Poliza__c WHERE NumeroPoliza__c =: user.get('NumeroPoliza') AND NumeroPoliza__c != NULL LIMIT 1];
                
                if (pol == null || pol.isEmpty()){ //No existe la póliza, la tengo que crear
                    Poliza__c newpol = new Poliza__c(Name = 'Poliza ' + user.get('NumeroPoliza'), NumeroPoliza__c = user.get('NumeroPoliza'), Contact__c = con.Id);
                    insert newpol;
                }else{ //existe, solo hay que actualizarla con el contacto
                    Poliza__c newpol = pol.get(0);
                    newpol.Contact__c = con.Id;
                    update newpol;
                }
            }

            update con;
        }
        //No exite un contacto pero si una poliza
        else if ( posiblePoliza != null && !posiblePoliza.isEmpty()){
            con.Id = posiblePoliza.get(0).Contact__c;
            
            System.debug('No existe un contacto pero si una poliza ' + posiblePoliza);
        }
        //No existe un contacto
        else{ 
            insert con;

            if (user.containsKey('NumeroPoliza')){ //Ha añadido una póliza
                List<Poliza__c> pol = [SELECT Id, Contact__c FROM Poliza__c WHERE NumeroPoliza__c =: user.get('NumeroPoliza') AND NumeroPoliza__c != NULL LIMIT 1];
                
                if (pol == null || pol.isEmpty()){ //No existe la póliza, la tengo que crear
                    Poliza__c newpol = new Poliza__c(Name = 'Poliza ' + user.get('NumeroPoliza'), NumeroPoliza__c = user.get('NumeroPoliza'), Contact__c = con.Id);
                    insert newpol;
                }else{ //existe, solo hay que actualizarla con el contacto
                    Poliza__c newpol = pol.get(0);
                    newpol.Contact__c = con.Id;
                    update newpol;
                }
            }
        }

        User userToInsert = new User();
        userToInsert.Username = user.get('Email');
        userToInsert.put('Email', user.get('Email'));
        userToInsert.FirstName = user.get('First Name');
        userToInsert.LastName = user.get('Last Name');
        userToInsert.LanguageLocaleKey = 'es';
        userToInsert.TimeZoneSidKey = 'Europe/Paris';
        userToInsert.EmailEncodingKey = 'UTF-8';
        userToInsert.CommunityNickname = randomStr(2) + user.get('Email').substringBefore('@');
        userToInsert.Alias = user.get('Last Name');
        userToInsert.LocaleSidKey = 'es';
        userToInsert.profileId = pro.Id;
        userToInsert.IsActive = true;
        userToInsert.ContactId = con.Id;
        insert userToInsert;

        return 'success';
        
    }

    public static String randomStr(Integer len) {
        final String chars = '123456789';
        String randStr = '';

        while (randStr.length() < len) {
            Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
            randStr += chars.substring(idx, idx+1);
        }
        return randStr; 
    }
}
